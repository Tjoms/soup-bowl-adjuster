#include <Servo.h>
#define button 12

Servo servo;
int pos = 0;


void setup() {
  servo.attach(2);

  pinMode(button, INPUT);

  Serial.begin(9600);

  pos = servo.read();
  printTiltPosition();


//Start position, adjust if nessesary
  pos = 174;
  servo.write(pos);
  printTiltPosition();
}


void loop() {
  
  //delay 100ms such that you have time to realease the button before starting a new action
  delay(100);
  waitForButtonPress();

  // Tilts the servo from LAST KNOWN position to one of your choosing; Range 180-0 and 0-180; 
  // This case: a 174 degeree tilt to a 160 degree tilt
  tilt(174, 160);
  
  delay(100);
  waitForButtonPress();

  tilt(160, 150);

  delay(100);
  waitForButtonPress();

  tilt(150, 174);

}


void waitForButtonPress() {
  while (digitalRead(button) == LOW) {
    //Wait for button press
  }
}

void tilt(int startDegree, int endDegree) {
  if (startDegree < endDegree) {
    Serial.println("LOW to HIGH");
    tiltLowToHigh(startDegree, endDegree);
  } else if (startDegree > endDegree) {
    Serial.println("HIGH to LOW");
    tiltHighToLow(startDegree, endDegree);
  }

}

void tiltLowToHigh(int startDegree, int endDegree) {
  for (pos = startDegree; pos <= endDegree; pos++) {
    servo.write(pos);
    printTiltPosition();
    delay(15);
  }
}

void tiltHighToLow(int startDegree, int endDegree) {
  for (pos = startDegree; pos >= endDegree; pos--) {
    servo.write(pos);
    printTiltPosition();
    delay(15);
  }
}

void printTiltPosition() {
  Serial.print("Servo is tilted : ");
  Serial.println(pos);
}
