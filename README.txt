:Author: Tjoms
:Email: marcus.alex@live.no
:Date: 29/08/2019
:Revision: 003
:License: Public Domain

= Project: Soup bowl adjuster

A Soup bowl adjuster is a machine that can tilt a bowl such as the liquid inside will 
gather together. This makes it easier to eat something like soup when there is verry little soup left.

== Step 1: Installation

1. Open this file
2. Edit as you like
3. Release to the World!

== Step 2: Assemble the circuit

1. Assemble the circuit following the diagram in Schematics.png attached to the sketch

== Step 3: Load the code

1. Upload the code contained in this sketch on to your board
2. Check if the servo is in the right position, if not try to edit the start position.
  2,1. If it does not work, try changing the position of the leaver that is attached on the servo gear  

=== Folder structure
....
 Talerkenvelter_003             => Arduino sketch folder
  ├── Talerkenvelter_003.ino    => main Arduino file
  ├── Schematics.png            => an image of the required schematics
  ├── Talerkenvelter.png        => an image of the layout
  └── ReadMe.adoc               => this file
....

=== License
This project is released under a {License} License.

=== Contributing
To contribute to this project please contact Tjoms https://id.arduino.cc/Tjoms

=== BOM


|===
| ID | Part name      | Part number | Quantity
| R1 | 10k Resistor   |             | 1
| S1 | 12V Servo      |             | 1
| A1 | Arduino Nano   |             | 1
| R1 | Push Button    |             | 1
| S1 | ON/OFf Switch  |             | 1
| A1 | 9V Battery     |             | 1
|===


=== Help
This document is written in the _AsciiDoc_ format, a markup language to describe documents.
If you need help you can search the http://www.methods.co.nz/asciidoc[AsciiDoc homepage]
or consult the http://powerman.name/doc/asciidoc[AsciiDoc cheatsheet]
